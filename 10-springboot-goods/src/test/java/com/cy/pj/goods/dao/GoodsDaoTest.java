package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsDaoTest {
    @Autowired
    private GoodsDao goodsDao;


    @Test
    void testDelete(){
        int row = goodsDao.deleteById(12);
        Assertions.assertEquals(true, row>=1);
    }

    @Test
    public void findGoodsDao(){
        List<Goods> goodsList = goodsDao.findGoods();
        for (Goods goods:goodsList) {
            System.out.println(goods);
        }
    }
}
