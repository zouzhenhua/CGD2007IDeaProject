package com.cy.pj.goods.dao;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;

@SpringBootTest
public class MyBatisTest {
    @Autowired
    private SqlSession sqlSession;

    @Test
    void testGetConnection(){
        Connection connection = sqlSession.getConnection();
//        System.out.println();
        Assertions.assertNotNull(connection);
    }
}
