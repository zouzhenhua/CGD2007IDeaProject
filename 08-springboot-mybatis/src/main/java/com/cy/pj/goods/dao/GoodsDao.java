package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper//用于描述持久层接口  告诉mybatis这个接口实现类由mybatis来创建并且将实现类交给spring管理
public interface GoodsDao {
    List<Goods> findGoods();
}
