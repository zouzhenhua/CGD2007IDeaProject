package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsDaoTest {
    @Autowired
    @Qualifier("goodsDaoImpl")
    private GoodsDao goodsDao;

//    @Autowired
//    private GoodsDaoImpl goodsDaoImpl;

    @Test
    void testFinGoods(){
        List<Goods> goods = goodsDao.findGoods();
        for (Goods good:goods) {
            System.out.println(good);
        }
    }
    @Test
    void testGoodsDaoImpl(){
        List<Goods> goodsList = goodsDao.findGoods();
        for (Goods goods:goodsList) {
            System.out.println(goods);
        }
    }

}
