package com.cy.pj.goods.dao;

import ch.qos.logback.classic.db.names.ColumnName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository //描述数据层实现类对象，本质上是一个特殊的@component
public class DefaultGoodsDao {
    @Autowired
    private DataSource dataSource;

    public List<Map<String,Object>> findGoods() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet res = null;
        String sql = "SELECT * FROM tb_goods";
        //1.获取连接
        try {
            connection = dataSource.getConnection();
            //2.创建statement对象
            statement = connection.createStatement();
            //3.发送sql
            res = statement.executeQuery(sql);
            //4.处理结果
            List<Map<String,Object>> mapList = new ArrayList<>();
            while (res.next()){
                mapList.add(rowMap(res));
            }
            return mapList;
        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException();
        }finally {
            //5.释放资源
            close(res, statement, connection);
        }
    }

    private Map<String,Object> rowMap(ResultSet res) throws SQLException {
        Map<String,Object> map = new HashMap<>();
//        map.put("id", res.getInt("id"));
//        map.put("name", res.getString("name"));
//        map.put("remark", res.getString("remark"));
//        map.put("createdTime", res.getTime("createdTime"));
        ResultSetMetaData rsmd = res.getMetaData();
        int columCount = rsmd.getColumnCount();
        for (int i = 0; i < columCount; i++) {
            map.put(rsmd.getColumnLabel(i+1), res.getObject(rsmd.getColumnLabel(i+1)));
        }

        return map;
    }

    /**
     * 释放资源的封装
     * @param res
     * @param state
     * @param conn
     */
    private void close(ResultSet res,Statement state,Connection conn){
        if (res!=null)try {res.close();}catch (Exception e){e.printStackTrace();}
        if (state!=null)try {res.close();}catch (Exception e){e.printStackTrace();}
        if (conn!=null)try {res.close();}catch (Exception e){e.printStackTrace();}
    }
}
