package com.cy.pj.goods.service;

import com.cy.pj.goods.pojo.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GoodsServiceTest {
    @Autowired
    private GoodsService goodsService;

    @Test
    void testFindGoods(){
        List<Goods> goodsList = goodsService.findGoods();
        for (Goods g:goodsList
             ) {
            System.out.println(g);
        }
    }
}
