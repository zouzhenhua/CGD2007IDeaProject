package com.cy.pj.module.controller;

import com.cy.pj.module.pojo.RequestParameter;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ParamObjectController {

    @GetMapping("/doParam01")
    public String doMethodParam(@RequestParam(required = false) String name) {//直接量接收请求参数,参数名要与请求参数名相同
        return "request params " + name;
    }

    @RequestMapping("/doParam02")
    public String doMethodParam(RequestParameter param) {//pojo对象接收请求参数,pojo对象中需提供与参数名相匹配的set方法
        return "request params " + param.toString();
    }

    @GetMapping("/doParam03")
    public String doMethodParam(@RequestParam Map<String, Object> param) {//map接收请求参数,必须使用@RequestParam对参数进行描述
        return "request params " + param.toString();
    }
}
