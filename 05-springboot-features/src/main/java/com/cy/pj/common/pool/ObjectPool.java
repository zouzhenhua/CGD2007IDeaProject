package com.cy.pj.common.pool;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

//@Scope("prototype")//prototype 表示多例作用域，此类实例与lazy无关默认何时需要何时加载，并不会存储到spring的bean池中
@Scope //默认值是singleton单例作用域
@Lazy//该注解描述的类可以延迟其创建，何时需要何时创建
@Component
public class ObjectPool {
    public ObjectPool(){
        System.out.println("ObjectPool()");
    }
    @PostConstruct
    void init(){
        System.out.println("init()");
    }
    @PreDestroy
    void desotry(){
        System.out.println("destory()");
    }
}
