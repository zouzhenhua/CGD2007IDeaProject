package com.cy.pj.goods.controller;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.misc.Contended;

import java.util.List;

@Controller
@RequestMapping("/goods/")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @RequestMapping("doFindGoodsById/{id}")
    String doFindGoodsById(@PathVariable Integer id,Model model){
        Goods goods = goodsService.FindGoodsById(id);
        model.addAttribute("g", goods);
        return "goods-update";
    }

    @RequestMapping("doGoodsAdd")
    public String doGoodsAdd(Goods goods){
        goodsService.insertGoods(goods);
        return "redirect:/goods/doGoodsUI";
    }

    @RequestMapping("doAddGoodsUI")
    public String doGoodsAddUI(){
        return "goods-add";
    }
    @RequestMapping("doDeleteById/{id}")
    public String deleteById(@PathVariable Integer id){
        goodsService.deleteByID(id);
        return "redirect:/goods/doGoodsUI";
    }

    @RequestMapping("doGoodsUI")
    private String doGoodsUI(Model model){
        List<Goods> goodsList = goodsService.findGoods();
        model.addAttribute("goodsList",goodsList);
        return "goods";
    }

}
