package com.cy.pj.module.pojo;

/**
 * 控制层用于封装响应结果,并输出到客户端的一个对象
 */
public class ResponseResult {

    private Integer code;
    private String message;
    //....

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
