package com.cy.pj.common.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @SpringBootTest描述的类也是交给spring管理的类
 */
@SpringBootTest
public class DefaultCacheTest {
    /**
     * @Autowired 注解描述的属性由Spring框架来按照一定的规则来为其注入值
     */
    @Autowired
    private  DefaultCache defaultCache;

    @Test
    public void testDefaultCache(){
        System.out.println(defaultCache.toString());
    }
}
