package com.cy.pj.module.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TemplatePageController {//Handler (通过此对象处理DispatcherServlet分发过来的请求)

    @RequestMapping("doHealthUI")
    public String doHealthUI() {
        return "default";
    }
}
