package com.cy.pj.goods.service;
import com.cy.pj.goods.pojo.Goods;
import java.util.List;

public interface GoodsService {
    List<Goods> findGoods();
    int deleteByID(Integer id);
    int insertGoods(Goods goods);
    Goods FindGoodsById(Integer id);
}
