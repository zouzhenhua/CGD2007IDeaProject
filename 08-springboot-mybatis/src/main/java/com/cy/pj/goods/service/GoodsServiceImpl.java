package com.cy.pj.goods.service;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements  GoodsService{
    private static  final Logger LOGGER = LoggerFactory.getLogger(GoodsServiceImpl.class);
    @Autowired
    @Qualifier("goodsDaoImpl")
    private GoodsDao goodsDao;

    @Override
    public List<Goods> findGoods() {
        long t1 = System.currentTimeMillis();
        List<Goods> goodsList = goodsDao.findGoods();
        long t2 = System.currentTimeMillis();
//        System.out.println("t2-t1:"+(t2-t1));
        LOGGER.info("findGoods()--->t2-t1={}", t2-t1);
        return goodsList;
    }

}
