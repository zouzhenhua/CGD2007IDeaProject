package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GoodsDaoImpl implements GoodsDao{

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public List<Goods> findGoods() {
//        List<Goods> goodsList = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.openSession();
        String statement = "com.cy.pj.goods.dao.GoodsDao.findGoods";
        List<Goods> goodsList =sqlSession.selectList(statement);
        sqlSession.close();

//        SqlSession sqlSession = sqlSessionFactory.openSession();
//        Connection connection = sqlSession.getConnection();
//        String sql = "select * from tb_goods";
//        try {
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(sql);
//            while (resultSet.next()){
//                Goods goods = new Goods();
//                goods.setId(resultSet.getInt("id"));
//                goods.setName(resultSet.getString("name"));
//                goods.setRemark(resultSet.getString("remark"));
//                goods.setCreateTime(resultSet.getTimestamp("createdTime"));
//                goodsList.add(goods);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        }finally {
//            try {
//                connection.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
        return goodsList;
    }
}
